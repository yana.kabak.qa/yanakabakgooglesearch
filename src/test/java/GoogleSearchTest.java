import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class GoogleSearchTest {
    GoogleSearch googleSearch = new GoogleSearch();
    PageHomeBrowser pageHomeBrowser;
    SearchForm searchForm;
    String result;
    List<WebElement> results;
    List<String> searchResult;
    private WebDriver webDriver;
    private final static String title = "Java";
    private boolean check;

    @Test
    public void findWordInGoogle(){
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        webDriver = new ChromeDriver();
        pageHomeBrowser = new PageHomeBrowser(webDriver);
        pageHomeBrowser.initialized();
        searchForm = new SearchForm(webDriver);
        searchForm.setName();
        result = searchForm.findResult();
        String title = googleSearch.searchInBrowser(pageHomeBrowser.search);
        Assert.assertEquals(title, result);
    }

    @Test
    public void findWordInLine(){
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        webDriver = new ChromeDriver();
        pageHomeBrowser = new PageHomeBrowser(webDriver);
        pageHomeBrowser.initialized();
        searchForm = new SearchForm(webDriver);
        searchForm.setName();
        searchForm.findResult();
        searchForm.submit();
        results = searchForm.findWords();
        for (WebElement webElement : results) {
            if(webElement.getText().contains(title)){
                check = true;
            }
        }
        Assert.assertTrue(check);
    }

    @After
    public void after(){
        webDriver.quit();
    }

}

