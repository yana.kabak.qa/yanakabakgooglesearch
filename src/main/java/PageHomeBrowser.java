import org.openqa.selenium.WebDriver;

public class PageHomeBrowser {
    WebDriver driver;
    SearchInfo search = new SearchInfo();

    public void initialized(){
        search.setBrowserName("https://www.google.com");
        driver.get(search.getBrowserName());
    }

    public PageHomeBrowser(WebDriver driver) {
        this.driver = driver;
    }
}
