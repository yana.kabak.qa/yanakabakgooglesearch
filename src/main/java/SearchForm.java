import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SearchForm {
    WebDriver driver;
    By name = By.name("q");
    By name1 = By.name("btnK");
    By xpath = By.xpath("//*[@id='rso']//h3/a");
    String wordSearch = "java";
    String findWord = "title";
    public SearchForm(WebDriver driver) {
        this.driver = driver;
    }
    public void setName (){
        driver.findElement(name).sendKeys(wordSearch);
    }
    public String findResult() { return driver.switchTo().activeElement().getAttribute(findWord); }
    public void submit() { driver.findElement(name1).submit();}
    public List<WebElement> findWords(){ return driver.findElements(xpath); }
}
