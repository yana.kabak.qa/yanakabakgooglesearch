import org.openqa.selenium.WebElement;

public class GoogleSearch {

    public String searchInBrowser(SearchInfo searchInfo){
        String failed = "failed";
        String passed = "Поиск";

        if(searchInfo.getBrowserName() == null && searchInfo.getKeyWord() == null && searchInfo.getProperty() == null){
            return failed;
        } else{
            return passed;
        }
    }
}
